﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float maxTime = 2f;
    public float maxTime2 = 4f;
    float timer = 0;
    float timer2 = 0;

    float yBarrier = -3f;
    public GameObject[] gameObjs = new GameObject[3];
    private static int countBarrier;

    float yBG = -.2f;
    public GameObject bg;
    public Sprite[] sprite = new Sprite[5];
    private static int countSpriteBG;

    float yB = -4f;
    public GameObject beforeG;

    void Start()
    {
        countSpriteBG = sprite.Length;
        GameObject newBG = Instantiate(bg);
        newBG.GetComponent<SpriteRenderer>().sprite = sprite[Random.Range(0, countSpriteBG)];
        newBG.transform.position = new Vector3(15, yBG, 1);

        countBarrier = gameObjs.Length;
        GameObject newObj = Instantiate(gameObjs[Random.Range(0, countBarrier)]);
        newObj.transform.position = new Vector3(15, yBarrier, -.2f);
    }

    void Update()
    {
        SpawnerObject();
        SpawnerB();

        timer += Time.deltaTime;
        timer2 += Time.deltaTime;
    }

    void SpawnerB()
    {
        if (timer2 > maxTime2)
        {
            GameObject newBefG = Instantiate(beforeG);
            newBefG.transform.position = new Vector3(25, yB, -1);
            Destroy(newBefG, 10);

            timer2 = 0;
            maxTime2 = Random.Range(3f, 12f);
        }

    }
    void SpawnerObject()
    {
        if (timer > maxTime)
        {
            GameObject newObj = Instantiate(gameObjs[Random.Range(0, countBarrier)]);
            newObj.transform.position = new Vector3(15, yBarrier, -.2f);
            Destroy(newObj, 10);

            GameObject newBG = Instantiate(bg);
            newBG.GetComponent<SpriteRenderer>().sprite = sprite[Random.Range(0, countSpriteBG)];
            newBG.transform.position = new Vector3(10, yBG, 1);

            timer = 0;
            maxTime = Random.Range(1f, 7f);
        }

    }



}
