﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameManager gameManager;
    public float speed = 10f;
    public float jumpForce = 10f;
    public bool isGrounded = false;
    public Transform groundCheck;
    public LayerMask whatIsGround;


    Rigidbody2D riba;
    Animator an;
    void Start()
    {
        riba = GetComponent<Rigidbody2D>();
        an = GetComponent<Animator>();
    }

    void Update()
    {
        Run();
        Jump();
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    void Run()
    {
        float hor = Input.GetAxis("Horizontal");
        riba.velocity = new Vector2(hor * speed, riba.velocity.y);
    }

    void Jump()
    {
        if (Input.GetMouseButtonDown(0) && isGrounded)
        {
            riba.velocity = Vector2.up * jumpForce;
            an.SetTrigger("Trig");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "block")
            gameManager.GameOver();
    }

    private void CheckGround()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, .3f, whatIsGround);

        if (!isGrounded) return;
    }
}
