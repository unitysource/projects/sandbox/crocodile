﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speedBush = 10f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveObj();   
    }

    void MoveObj()
    {
        transform.position += Vector3.left * (speedBush * Time.deltaTime);
    }
}
